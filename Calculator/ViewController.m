//
//  ViewController.m
//  Calculator
//
//  Created by Anil Kumar on 9/9/15.
//  Copyright (c) 2015 Anil Kumar. All rights reserved.
//

#import "ViewController.h"


@interface ViewController () {
    
    UILabel *llbResult;
    
    UITextField *txtInput;
    
    UIButton *btnMulti;
    
    NSInteger actionType;
    
    NSString *prevalue;
    NSString *btnaction;
    //NSMutableString *number;
    float result;
    
    int currentOperation;
//    NSInteger first_Value;
//      NSInteger sec_Value;
//      NSInteger operation;
    float behindTheDecimal;
    
    
    float b ;
    float firstValue;
    float secondValue;
    float currentValue;
    float a;
    float preoparation;
    int currentNumber;
    
    BOOL isAnyOperatorClicked;
    
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViewLayout];
    // Do any additional setup after loading the view, typically from a nib.
//}

//- (void)equal:(UIButton *)sender {
//        secondValue = [txtInput.text floatValue ];
//    
//    if ([sender tag] == 19) {
//        
        //NSInteger element =[ self performOperation : (NSInteger)operation first_Value:(NSInteger)first_Value sec_Value:(NSInteger)sec_Value];
//        switch (actionType) {
//            case 1:
//                secondValue = firstValue + secondValue;
//                
//                
//                txtInput.text = [NSString stringWithFormat:@"%.1f",secondValue];
//                
//                break;
//                
//            case 2:
//                a = firstValue - a;
//                txtInput.text = [NSString stringWithFormat:@"%.1f",a];
//                break;
//                
//            case 3:
//                
//                secondValue = firstValue * secondValue;
//                txtInput.text = [NSString stringWithFormat:@"%.1f",secondValue];
//                
//                firstValue = secondValue;
//                
//                secondValue = 0;
//                break;
//            case 4:
//                a = firstValue / a;
//                txtInput.text = [NSString stringWithFormat:@"%.1f",a];
//                break;
//                
//            default:
//                break;
//        }
//    }

    isAnyOperatorClicked = NO;
    
    return;
    
}


- (void)setupViewLayout {
    
    isAnyOperatorClicked = NO;
    
    UIView *view1 =[UIView new];
    view1.frame = CGRectMake(0, 0, 375, 667);
    view1. backgroundColor = [UIColor blackColor];
    [self.view addSubview:view1];
    
    firstValue = -1;
    actionType = -1;
    currentNumber = 0;
   
  
    
    /*
     llbResult = [UILabel new];
     llbResult.frame = CGRectMake(2, 1, [[UIScreen mainScreen] bounds].size.width - 4, 167);
     
     //iPhone 4
     if ([[UIScreen mainScreen] bounds].size.height == 480) {
     }
     //iPhone 5
     else if ([[UIScreen mainScreen] bounds].size.height == 568) {
     
     }
     
     llbResult.backgroundColor = [UIColor colorWithRed:140/255.0 green:142/255.0 blue:144/255.0 alpha:1];
     llbResult.text = @"Result";
     llbResult.font = [UIFont fontWithName:@"Georgia" size:60.0];
     llbResult.textColor = [UIColor whiteColor];
     llbResult.textAlignment = NSTextAlignmentRight;
     [self.view addSubview:llbResult];
     
     */
    
    txtInput = [UITextField new];
    txtInput.frame = CGRectMake(2, 1, [[UIScreen mainScreen] bounds].size.width - 4, 167);
    //iPhone 4
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        
    }
    //iPhone 5
    else if ([[UIScreen mainScreen] bounds].size.height == 568) {
        
    }
    txtInput.backgroundColor = [UIColor grayColor];
    txtInput.textAlignment = NSTextAlignmentRight;
    txtInput.font = [UIFont fontWithName:@"Georgia" size:60.0];
    txtInput. textColor = [UIColor whiteColor];
    [self.view addSubview:txtInput];
    
    
    UIButton*btn1 = [UIButton new];
    btn1.frame = CGRectMake(2, 445,91,91);
    btn1.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn1 setTitle:@"1" forState:UIControlStateNormal];
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn1.tag = 1;
    [btn1 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn1];
    
    
    
    UIButton*btn2 = [UIButton new];
    btn2.frame = CGRectMake(94, 445,91,91);
    btn2.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn2 setTitle:@"2" forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn2.tag = 2;
    [btn2 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn2];
    
    
    
    UIButton*btn3 = [UIButton new];
    btn3.frame = CGRectMake(186, 445,91,91);
    btn3.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn3 setTitle:@"3" forState:UIControlStateNormal];
    [btn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn3.tag = 3;
    [btn3 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn3];
    
    
    
    UIButton*btn4 = [UIButton new];
    btn4.frame = CGRectMake(2, 353, 91, 91);
    btn4.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn4 setTitle:@"4" forState:UIControlStateNormal];
    [btn4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn4.tag = 4;
    [btn4 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn4];
    
    
    
    UIButton*btn5 = [UIButton new];
    btn5.frame = CGRectMake(94, 353, 91, 91);
    btn5.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn5 setTitle:@"5" forState:UIControlStateNormal];
    [btn5 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn5.tag = 5;
    [btn5 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn5];
    
    
    
    UIButton*btn6 = [UIButton new];
    btn6.frame = CGRectMake(186, 353, 91, 91);
    btn6.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn6 setTitle:@"6" forState:UIControlStateNormal];
    [btn6 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn6.tag = 6;
    [btn6 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn6];
    
    
    
    UIButton*btn7 = [UIButton new];
    btn7.frame = CGRectMake(2, 261, 91, 91);
    btn7.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn7 setTitle:@"7" forState:UIControlStateNormal];
    [btn7 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn7.tag = 7;
    [btn7 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn7];
    
    
    
    UIButton*btn8 = [UIButton new];
    btn8.frame = CGRectMake(94, 261, 91, 91);
    btn8.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn8 setTitle:@"8" forState:UIControlStateNormal];
    [btn8 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn8.tag = 8;
    [btn8 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn8];
    
    
    
    UIButton*btn9 = [UIButton new];
    btn9.frame = CGRectMake(186, 261, 91, 91);
    btn9.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn9 setTitle:@"9" forState:UIControlStateNormal];
    [btn9 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn9.tag = 9;
    [btn9 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn9];
    
    
    UIButton*btn0 = [UIButton new];
    btn0.frame = CGRectMake(2, 537, 183, 91);
    btn0.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btn0 setTitle:@"0" forState:UIControlStateNormal];
    [btn0 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn0.tag = 0;
    [btn0 addTarget:self action:@selector(btnValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn0];
    
    
    UIButton*btnPoint = [UIButton new];
    btnPoint.frame = CGRectMake(186, 537, 91, 91);
    btnPoint.backgroundColor = [UIColor colorWithRed:224/255.0 green:225/255.0 blue:224/255.0 alpha:1];
    [btnPoint setTitle:@"." forState:UIControlStateNormal];
    [btnPoint setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnPoint.tag = 11;
    [btnPoint addTarget:self action:@selector(buttonDecialPointPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnPoint];
    
    
    UIButton*btnClear = [UIButton new];
    /*
     
     int width =  [[UIScreen mainScreen] bounds].size.width - 270;
     int height = [[UIScreen mainScreen] bounds].size.height -390;
     
     */
    btnClear.frame = CGRectMake(2, 169, 91,91);
    btnClear.backgroundColor =
    [UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1];
    [btnClear setTitle:@"AC" forState:UIControlStateNormal];
    [btnClear setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnClear.tag = 12;
    [btnClear addTarget:self action:@selector(btnValue: ) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnClear];
    
    
    UIButton*btnPlusMinus = [UIButton new];
    btnPlusMinus.frame = CGRectMake(94, 169, 91,91);
    btnPlusMinus.backgroundColor = [UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1];
    [btnPlusMinus setTitle:@"+/-" forState:UIControlStateNormal];
    [btnPlusMinus setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [btnPlusMinus setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnPlusMinus.tag = 13;
    [btnPlusMinus addTarget:self action:@selector(PlusMinus) forControlEvents:UIControlEventTouchUpInside];
    [btnPlusMinus setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.view addSubview:btnPlusMinus];
    
    
    
    UIButton*btnPercentage = [UIButton new];
    btnPercentage.frame = CGRectMake(186, 169, 91, 91);
    btnPercentage.backgroundColor = [UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1];
    [btnPercentage setTitle:@"%" forState:UIControlStateNormal];
    [btnPercentage setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnPercentage.tag = 14;
    [btnPercentage addTarget:self action:@selector(Percentage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnPercentage];
    
    
    btnMulti = [UIButton new];
    btnMulti.frame = CGRectMake(278, 261, 95, 91);
    btnMulti.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnMulti setTitle:@"x" forState:UIControlStateNormal];
    [btnMulti setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnMulti.tag = 15;
   // currentOperation = 3;
    [btnMulti addTarget:self action:@selector(btnclicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMulti];
    
    
    UIButton*btnDiv = [UIButton new];
    btnDiv.frame = CGRectMake(278, 169, 95, 91);
    btnDiv.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnDiv setTitle:@"/" forState:UIControlStateNormal];
    [btnDiv setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnDiv.tag = 16;
   // currentOperation = 4;
    [btnDiv addTarget:self action:@selector(btnclicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDiv];
    
    
    UIButton*btnAdd = [UIButton new];
    btnAdd.frame = CGRectMake(278,445,95,91);
    btnAdd.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnAdd setTitle:@"+" forState:UIControlStateNormal];
    [btnAdd setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnAdd.tag = 17;
    //currentOperation = 1;
    [btnAdd addTarget:self action:@selector(btnclicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnAdd];
    
    
    
    UIButton*btnSub = [UIButton new];
    btnSub.frame = CGRectMake(278, 353, 95, 91);
    btnSub.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnSub setTitle:@"-" forState:UIControlStateNormal];
    [btnSub setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnSub.tag = 18;
    //currentOperation = 2;
    [btnSub addTarget:self action:@selector(btnclicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSub];
    
    
    UIButton*btnequal = [UIButton new];
    btnequal.frame = CGRectMake(278, 537, 95, 91);
    btnequal.backgroundColor = [UIColor colorWithRed:247/255.0 green:146/255.0 blue:50/255.0 alpha:1];
    [btnequal setTitle:@"=" forState:UIControlStateNormal];
    [btnequal setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnequal.tag = 19;
    [btnequal addTarget:self action:@selector(equal:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnequal];
    
}

- (void)btnValue: (id) sender{
    
   
    currentNumber = currentNumber *10 + (int)[sender tag];
    txtInput.text = [NSString stringWithFormat:@"%d", currentNumber];
    
  /*if ([sender tag] == 1){
        
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"1";
        }
    }
    else if ([sender tag] == 2){
        
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"2";
        }
    }
    else if ([sender tag] == 3){
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"3";
        }
        

    }
    else if ([sender tag] == 4){
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"4";
        }
        
    }
    else if ([sender tag] == 5){
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"5";
        }
    }
   
    else if ([sender tag] == 6){
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"6";
        }
        
    }
    else if ([sender tag] == 7){
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"7";
        }
        
    }
    else if ([sender tag] == 8){
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"8";
        }
        
    }
    else if ([sender tag] == 9){
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"9";
        }
        
    }
    else if ([sender tag] == 0){
        if ([txtInput.text length] > 0) {
            
            NSString *prevValue = txtInput.text;
            txtInput.text = [NSString stringWithFormat:@"%@%ld",prevValue,[sender tag]];
        }
        else {
            txtInput.text = @"0";
        }
        
    }
    else if ([sender tag]== 12 ){
        txtInput.text = @"";
        firstValue = -1;
        prevalue = @"";
        secondValue = -1;
        
    }*/
    
//    else if ([sender tag] == 11){
//        if ([txtInput.text length] > 0) {
//            
//            NSString *prevValue = txtInput.text;
//            txtInput.text = [NSString stringWithFormat:@"%@%.",prevValue,[sender tag]];
//        }
//        else {
//            txtInput.text = @".";
//        }
//        
//    }
    
//    if (isAnyOperatorClicked == YES) {
//        
//        NSInteger newValue = [txtInput.text integerValue];
//        
//        firstValue = [self performOperation:actionType first_Value:firstValue sec_Value:newValue];
//        txtInput.text = [NSString stringWithFormat:@"%.1f",firstValue];
//        
//        isAnyOperatorClicked = NO;
//        secondValue = -1;
//    }
}

- (NSInteger)performOperation:(NSInteger)operation first_Value:(NSInteger)first_Value sec_Value:(NSInteger)sec_Value {
    
    switch (operation) {
      
        //Multiplication
        case 15:
            return first_Value*sec_Value;
            //txtInput.text = [NSString stringWithFormat:@"%ld",first_Value];
            break;
            case 16:
            return first_Value/sec_Value;
            break;
            case 17:
            return first_Value + sec_Value;
            break;
            case 18:
            return first_Value - sec_Value;
            break;

        default:
            break;
    }
    
    return 0;
}

/*
- (void
) buttonDecimalPointPressed:(id)sender
{
    if ([txtInput.text length] > 0) {
        
        NSString *prevValue = txtInput.text;
         prevalue = [prevalue appendString:@"."];

    if(behindTheDecimal == YES)
        return;
    behindTheDecimal = YES;
    prevalue = [prevalue appendString:@"."];
    txtInput.text = prevalue;
}
}*/
//-(void)(UIButton*) sender{
    
-(void)btnclicked:(id)sender {
   /*
    //currentOperation = (int)[sender tag];
    if (currentOperation == 0) {
        
        currentNumber = [txtInput.text floatValue];
        txtInput.text = @"";
    }
    else if (([txtInput.text length]>0) && (currentNumber > 0)) {
      
        result = [txtInput.text floatValue];
        
            switch (currentOperation) {
                case 17:
                    result = result + currentNumber;
                    break;
                case 18:
                    result = result - currentNumber;
                    break;
                case 15:
                    result = result * currentNumber;
                    break;
                case 16:
                    result = result / currentNumber;
                    break;
                default:
                    currentOperation = 0;
                    break;
                    
            }
        }
        
        currentNumber = 0;
        txtInput.text = [NSString stringWithFormat:@"%.1f", result];
        if ([sender tag] ==0) result=0;
        currentOperation = (int) [sender tag];
        
    }
    
    
*/
    
    isAnyOperatorClicked = YES;{
    
    
    
    
//txtInput.text = @"";
    if ([sender tag] == 15){
        //actionType = 3;
        
        if (firstValue == -1) {
            
            firstValue = [txtInput.text floatValue];
            txtInput.text = @"";
            //[self btnValue];
        }
        if (([txtInput.text length]>0) && (firstValue > 0)) {
            txtInput.text = @"";
            
            

//            secondValue = [txtInput.text floatValue];
//            currentValue = firstValue * secondValue;
//            txtInput.text = [NSString stringWithFormat:@"%f",currentValue];
//            firstValue = currentValue;
//            txtInput.text = @"";
            
            
            
        }
    }
    
    
    
    else if ([sender tag] == 16) {
        actionType = 4;
        if (firstValue == -1) {
            
            firstValue = [txtInput.text floatValue];
            txtInput.text = @"";
        }
        else if (([txtInput.text length]>0) && (firstValue > 0)) {
            secondValue = [txtInput.text floatValue];
            firstValue = firstValue / secondValue;
            txtInput.text = @"";
            
            
        }
        
    }
    
    
    else if ([sender tag] == 17){
       // actionType = 1;
        if (firstValue == -1) {
            
            firstValue = [txtInput.text floatValue];
            txtInput.text = @"";
        }
        
        else if (([txtInput.text length]>0) && (firstValue > 0)) {
            txtInput.text = @"";
//            [self operatorclicked];
//            secondValue = [txtInput.text floatValue];
//            currentValue = firstValue + secondValue;
//            txtInput.text = [ NSString stringWithFormat:@"%f",currentValue];
//            firstValue = currentValue;
//            txtInput.text = @"";
            
            
        }
        
    }
    
    
    else if ([sender tag] == 18){
        actionType = 2;
        if (firstValue == -1) {
            
            firstValue = [txtInput.text floatValue];
            txtInput.text = @"";
        }
        else if (([txtInput.text length]>0) && (firstValue > 0)) {
            secondValue = [txtInput.text floatValue];
            firstValue = firstValue - secondValue;
            txtInput.text = @"";
        }
    }
    
    actionType = [sender tag];
        

   }
}

//-(void) operatorclicked {
//    if (isAnyOperatorClicked == YES) {
//
//        NSInteger newValue = [txtInput.text integerValue];
//
//        firstValue = [self performOperation:actionType first_Value:firstValue sec_Value:newValue];
//        txtInput.text = [NSString stringWithFormat:@"%.1f",firstValue];
//
//        isAnyOperatorClicked = NO;
//        secondValue = -1;
//    }
//    }
//    
//}
//}
/*
- (void)btnAction:(UIButton *) sender {
    
    actionType = (int)[sender tag];
    
    if ( actionType == -1){
            [self btnclicked:sender];
    }
    else
    {
        [self btnclicked:sender];
    }
}*/

/*-(void) btnclear: (id) sender {
    
    if ([sender tag]== 12 ){
        txtInput.text = @"";
        currentNumber = @"";
        result = 0;
     
    }

}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
}


@end
